from pydub import AudioSegment

sound = AudioSegment.from_wav("no_confirma.wav")
sound = sound.set_frame_rate(8000)
sound = sound.set_channels(1)
sound.export("no_confirma.wav", format="wav")

sound = AudioSegment.from_wav("no_gracias.wav")
sound = sound.set_frame_rate(8000)
sound = sound.set_channels(1)
sound.export("no_gracias.wav", format="wav")

sound = AudioSegment.from_wav("si_transfiere.wav")
sound = sound.set_frame_rate(8000)
sound = sound.set_channels(1)
sound.export("si_transfiere.wav", format="wav")