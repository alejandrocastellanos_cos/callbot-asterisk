#!/usr/bin/python
# coding: utf-8

import os
import boto3
from pydub import AudioSegment
from secrets import token_urlsafe

number = str(3057131877) #str(sys.argv[1])
orden = str(1235)
fecha_visita = "20-10-2019"
franja = "10 a m"
direccion = "calle 150 numero 20 30"

ubicacion_audios_tts = '/home/'

#conectar a aws
polly = boto3.client('polly')

#tts orden
orden_spoken_text = polly.synthesize_speech(Text=orden, OutputFormat='mp3', VoiceId='Mia', SampleRate='8000')
audio_orden = ubicacion_audios_tts+token_urlsafe(7)+'.mp3'
with open(audio_orden, 'wb') as f:
    f.write(orden_spoken_text['AudioStream'].read())
    f.close()

#tts fecha_visita
fecha_visita_spoken_text = polly.synthesize_speech(Text=fecha_visita, OutputFormat='mp3', VoiceId='Mia', SampleRate='8000')
audio_fecha_visita = ubicacion_audios_tts+token_urlsafe(7)+'.mp3'
with open(audio_fecha_visita, 'wb') as f:
    f.write(fecha_visita_spoken_text['AudioStream'].read())
    f.close()

#tts franja visita
franja_visita_spoken_text = polly.synthesize_speech(Text=franja, OutputFormat='mp3', VoiceId='Mia', SampleRate='8000')
audio_franja_visita = ubicacion_audios_tts+token_urlsafe(7)+'.mp3'
with open(audio_franja_visita, 'wb') as f:
    f.write(franja_visita_spoken_text['AudioStream'].read())
    f.close()

#tts direccion
direccion_visita_spoken_text = polly.synthesize_speech(Text=direccion, OutputFormat='mp3', VoiceId='Mia', SampleRate='8000')
audio_direccion = ubicacion_audios_tts+token_urlsafe(7)+'.mp3'
with open(audio_direccion, 'wb') as f:
    f.write(direccion_visita_spoken_text['AudioStream'].read())
    f.close()

#unión audio final
audio_inicio_1 = AudioSegment.from_file("/var/lib/asterisk/agi-bin/callbot-eagi/Aws/Base_sounds/inicio_1.wav")
audio_orden_1 = AudioSegment.from_file(audio_orden)
audio_inicio_2 = AudioSegment.from_file("/var/lib/asterisk/agi-bin/callbot-eagi/Aws/Base_sounds/inicio_2.wav")
audio_fecha_visita_2 = AudioSegment.from_file(audio_fecha_visita)
audio_inicio_3 = AudioSegment.from_file("/var/lib/asterisk/agi-bin/callbot-eagi/Aws/Base_sounds/inicio_3.wav")
audio_franja_visita_3 = AudioSegment.from_file(audio_franja_visita)
audio_inicio_4 = AudioSegment.from_file("/var/lib/asterisk/agi-bin/callbot-eagi/Aws/Base_sounds/inicio_4.wav")
audio_direccion_3 = AudioSegment.from_file(audio_direccion)
audio_inicio_5 = AudioSegment.from_file("/var/lib/asterisk/agi-bin/callbot-eagi/Aws/Base_sounds/inicio_5.wav")
combined = audio_inicio_1 + audio_orden_1 + audio_inicio_2 + audio_fecha_visita_2 + audio_inicio_3 + audio_franja_visita_3 + audio_inicio_4 + audio_direccion_3 + audio_inicio_5
#exportar nuevo audio
combined.export(ubicacion_audios_tts+number+".wav", format='wav')

#convertir audio a 8000hz y mono
sound = AudioSegment.from_wav(ubicacion_audios_tts+number+".wav")
sound = sound.set_frame_rate(8000)
sound = sound.set_channels(1)
sound.export(ubicacion_audios_tts+number+".wav", format="wav")